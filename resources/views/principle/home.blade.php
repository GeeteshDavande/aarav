<div class="text-left">
Hii, {{$username}}
</div>
<div class="text-right" style="text-align: right;">
	<a type="button" class="btn btn-success" href="{{ url('logout') }}">Logout</a>
</div>
<hr>
<br/>
<br/>

<table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Enrollment No</th>
                <th>Class</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($user as $data)
            <tr>
                <td>{{$data->name}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->enroll_no}}</td>
                <td>{{$data->standard}}</td>
                
                <td>
                	<select class="prin_status" name="prin_status" id="prin_status" onchange="getval(this,{{$data->id}});">
                		<option value="">Please select</option>
                		@foreach($status as $st)
                		<option value="{{$st['id']}}" <?php echo ($data->status == $st['id'])?"selected":"" ?>>{{$st['status_name']}}</option>
                		@endforeach
                	</select>
                	<input type="hidden" name="hidden_stand" id="hidden_stand" value="">
                	<input type="hidden" name="hidden_user_id" id="hidden_user_id" value="{{$data->id}}">
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    	$.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    	$('#example').DataTable();

	});

	function getval(sel,user_id)
		{	
			$('#hidden_stand').val(sel.value);
			//var user_id = $('#hidden_user_id').val();
			var stand = $('#hidden_stand').val();
			$.ajax({
                        type: "POST",
                        datatype:"json",
                        data: {
                        	status:stand,
                        	user_id:user_id,
                        	"_token": "{{ csrf_token() }}"
                        },
                        url: "update_status_principle",
                              success: function(data)
                              {
                                if(stand == '2'){
                                	alert("Request Approval Successfully...");
                                }else if(stand == '3'){
                                   alert("Request Disapproval Successfully...");
                                }
                                
                              }
                        });
			
		}
    </script>
    <style type="text/css">
    	.dataTables_wrapper{
    		padding: 40px;
    	}
    </style>