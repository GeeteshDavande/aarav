<div class="text-left">
Hii, {{$username}}
</div>
<div class="text-right" style="text-align: right;">
	<a type="button" class="btn btn-success" href="{{ url('logout') }}">Logout</a>
</div>
<hr>
<br/>
<br/>

<table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Enrollment No</th>
                <th>Class</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($user as $data)
            <tr>
                <td>{{$data->name}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->enroll_no}}</td>
                <td>
                	<select class="standard" name="standard" id="standard" onchange="getval(this);">
                		<option value="">Please select</option>
                		@foreach($standard as $st)
                		<option value="{{$st['id']}}" <?php echo ($data->class == $st['id'])?"selected":"" ?>>{{$st['standard']}}</option>
                		@endforeach
                	</select>
                	<input type="hidden" name="hidden_stand" id="hidden_stand" value="">
                </td>
                @if($data->status == '1')
                <td><a type="button" class="btn btn-success">Pending</a></td>
                @elseif($data->status == '2')
                <td><a type="button" class="btn btn-success">Approved</a></td>
                @elseif($data->status == '3')
                <td><a type="button" class="btn btn-danger">DisApproved</a></td>
                @else
                <td><a type="button" class="btn btn-success" onclick="status('Pending');">Submit</a></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

    	$.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    	$('#example').DataTable();

	});

	function getval(sel)
		{
			$('#hidden_stand').val(sel.value);
		}

		function status(val)
		{
			var stand = $('#hidden_stand').val();
			if(stand != ''){
			$.ajax({
                        type: "POST",
                        datatype:"json",
                        data: {
                        	status:val,
                        	stand:stand,
                        	"_token": "{{ csrf_token() }}"
                        },
                        url: "update_status",
                              success: function(data)
                              {
                                if(data != ''){
                                	alert("Request Send for Approval...");
                                }else{
                                   alert("something went please try again...");
                                }
                                
                              }
                        });
			}else{
				alert("Please select Class");
			}
		}
    </script>
    <style type="text/css">
    	.dataTables_wrapper{
    		padding: 40px;
    	}
    </style>