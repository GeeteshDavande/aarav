<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: white;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
  border: 1px solid black;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body style="padding: 80px 300px;">

<form role="form" id="register"><!-- method="POST" action="{{ url('register') }}" -->
  {{ csrf_field() }}
  <input type="hidden" name="redirect_url" value="{{ request('redirect_url', '/login') }}">
  <div class="container">
    <h1>Register</h1>
    <hr>

    <div class="form-group">
    <label for="email"><b>Name</b></label>
    <input type="text" placeholder="Enter Name" name="name" id="name" required>
    </div>
    
    <div class="form-group">
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" id="email" required>
    </div>

    <div class="form-group">
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" id="password" required>
    </div>

    <hr>
    <button type="submit" class="registerbtn">Register</button>
  </div>
  
  <div class="container signin">
    <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>
</form>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script>
  $(document).ready(function(){
      $.ajaxSetup(
            {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        );
  $('#register').validate({
                rules: {
                  name: {
                    required: true,
                    minlength:3,
                    maxlength:50
                  },
                  email:{
                    required:true,
                    maxlength:50,
                    email:true
                  },
                  password:{
                    required:true,
                    minlength : 5
                  }
                },  
                  messages: {
                  name: {
                    required: "Please Enter Name."
                  },
                  email:{
                    required: "Please enter Email."
                  },
                  password:{
                    required: "Please enter Password"
                  }
                },
                submitHandler: function (form) {
                  var formData = new FormData($('#register')[0]);

                       $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        cache: false,
                        data: formData,
                        url: "register_add",
                              success: function(data)
                              {
                                if(data == 'success'){
                                  alert("Registerd successfully...");
                                  window.location="/login";
                                }else if(data == 'exists'){
                                  //swal("Exists","Email Id Already Exists","error");
                                  alert("Email Id Already Exists");
                                }else{
                                  alert("something went please try again...");
                                }
                                
                              }
                        });
                }
  });

});
</script>
<style type="text/css">
  .error{
    color: red;
  }
</style>
</body>
</html>
