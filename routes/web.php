<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
Route::any('logout', 'Auth\LoginController@logout');

$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');

Route::post('register_add', 'Auth\RegisterController@register');


Route::group(
    ['middleware' => ['principle'], 'prefix' => 'principle', 'as' => 'principle.'], function () {

	Route::get('home', 'Principle\PrincipleController@index');
	Route::post('update_status_principle', 'Principle\PrincipleController@update_status_principle');

});

Route::group(
    ['middleware' => ['registar'], 'prefix' => 'registar', 'as' => 'registar.'], function () {

	Route::get('index', 'Registar\RegistarController@index'); 
	Route::post('update_status', 'Registar\RegistarController@update_status');

});