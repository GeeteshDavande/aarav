<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class user_standard extends Model
{
	protected $table="user_standard";
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function InsertUserStandard($input)
    {   
    	$data = new user_standard;
        $data->user_id = session('user_id');
        $data->status = '1'; //pending
        $data->class = $input['stand'];
        $data->save();
        if(!empty($data->id)){
            return $data->id;
        }else{
            return '';
        }
    }

    public function UpdateRequestUser($input)
    {
        DB::table('user_standard')
                ->where('user_id', $input['user_id'])
                ->update(['status' => $input['status']]);
    }

}