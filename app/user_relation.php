<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class user_relation extends Model
{
	protected $table="user_type_relation";
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function UserRelation($userId)
    {
    	$data = new user_relation;
        $data->user_id = $userId;
        $data->user_type_id = '2';
        $data->save();
    }

}