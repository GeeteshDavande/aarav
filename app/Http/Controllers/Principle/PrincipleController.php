<?php
/**
 * Config File

 * Course controller file.

 * PHP version 5.4

 * @category   Components
 * @package    Laravel
 * @subpackage Excel
 * @author     Manasi <manasi@truetest.in>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://yoursite.com
 * @since      5.4
 */
namespace App\Http\Controllers\Principle;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Standard;
use App\user_standard;
use App\status;

/**
 * MyClass Class Doc Comment
 *
 * @category Class
 * @package  MyPackage
 * @author   Manasi <manasi@truetest.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class PrincipleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request,user_standard $user_standard)
    {
        $this->middleware('auth');
        $this->user_standard = $user_standard;
    }

    public function index(Request $request)
    {   
        $username = session('user_name');
        $user = User::getUserDataMain();
        $status = status::orderBy("id", "asc")->get()->toArray();
        return view('principle.home',compact('username','user','status'));
    }

    public function update_status_principle(Request $request)
    {
        $input = $request->all();
        return $this->user_standard->UpdateRequestUser($input);
    }
    
}
