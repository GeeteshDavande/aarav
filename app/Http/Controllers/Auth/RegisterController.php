<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

use App\User;
use App\user_relation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Hash;
use Redirect;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/registar/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request,User $User,user_relation $user_relation)
    {
        $this->middleware('guest');
        $this->user = $User;
        $this->user_relation = $user_relation;
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'enroll_no' => rand(1111111111,9999999999),
        ]);*/
    }

    public function register(Request $request)
    {
        $users = User::where('email',$request->email)->get()->toArray();
        if(count($users)>0){
            return 'exists';
        }else{
            $input = $request->all();
            $userId = $this->user->createUser($input);
            $this->user_relation->UserRelation($userId);
            //event(new Registered($user = $this->create($request->all())));
            if(!empty($userId)){
                return 'success';
            }else{
                return 'error';
            }
            
        }
        //$this->validator($request->all())->validate();

        //event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        //return $this->registered($request, $user)
          //  ?: redirect($request->input('redirect_url'));
    }

}
