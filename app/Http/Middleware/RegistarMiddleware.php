<?php
/**
 * Config File

 * Course controller file.

 * PHP version 5.4

 * @category   Components
 * @package    Laravel
 * @subpackage Excel
 * @author     Manasi <manasi@truetest.in>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://yoursite.com
 * @since      5.4
 */
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
/**
 * MyClass Class Doc Comment
 *
 * @category Class
 * @package  MyPackage
 * @author   Manasi <manasi@truetest.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class RegistarMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request comment
     * @param \Closure                 $next    comment
     * @param string|null              $guard   comment
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user_type_id=Session::get('user_type_id');
        if($user_type_id!='2' || !Auth::check()){
            return redirect('/');
        }
        return $next($request);
    }
}
