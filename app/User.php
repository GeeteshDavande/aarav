<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','enroll_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table="users";
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] 
            = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $query = $this->createModel()->newQuery();

        foreach ($credentials as $key => $value) {
            if (! Str::contains($key, 'password')) {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }    

    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

    public function createUser($input)
    {
        $data = new User;
        $data->name = $input['name'];
        $data->email = $input['email'];
        $data->password = Hash::make($input['password']);
        $data->enroll_no = rand(1111111111,9999999999);
        $data->save();
        if(!empty($data->id)){
            return $data->id;
        }else{
            return '';
        }
    }

    public static function getUserData($user_id)
    {
        return DB::table('users as u')
            ->select('u.id','u.name','u.email','u.enroll_no','us.class','us.status')
            ->leftJoin('user_standard as us', function($join) {
                        $join->on('us.user_id', '=', 'u.id');
                    })
            ->where('u.id',$user_id)
            ->get()
            ->toArray();
    }

    public static function getUserDataMain()
    {
        return DB::table('users as u')
            ->select('u.id','u.name','u.email','u.enroll_no','us.class','us.status','s.standard')
            ->leftJoin('user_standard as us', function($join) {
                        $join->on('us.user_id', '=', 'u.id');
                    })
            ->leftJoin('standard as s', function($join) {
                        $join->on('s.id', '=', 'us.class');
                    })
            ->where('u.id','!=','1')
            ->get()
            ->toArray();
    }


}
